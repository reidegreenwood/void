# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias xinstall='sudo xbps-install'
alias xsync='sudo xbps-install -S'
alias xupdate='sudo xbps-install -Su'
alias xsearch='sudo xbps-query -Rs'
alias xdown='sudo xbps-install -Sf'
alias xinfo='sudo xbps-query -R'
alias xlistrepo='sudo xbps-query -L'
alias xlistpkg='sudo xbps-query -l'
alias xlistfile='sudo xbps-query -R -f'
alias xlistdep='xbps-query -R -x'
alias xlist-reversedep='xbps-query -R -X'
alias xremove='sudo xbps-remove'
alias xremove-all='sudo xbps-remove -R'
alias xcache='sudo xbps-remove -O' 
alias xclean='sudo xbps-remove -o' 
alias xclean-all='sudo xbps-remove -Oo'
alias xreconfigure-pkg='sudo xbps-reconfigure'
alias xreconfigure-all='sudo xbps-reconfigure -a' 
alias xreconfigure-force='sudo xbps-reconfigure -f'
alias reboot='sudo reboot'
alias shutdown='sudo shutdown -h now'
#PS1='[\u@\h \W]\$ '
PS1='\$ '
ufetch